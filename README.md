# Peach Payments hosted GraphQL Magento 2 module

## Installation

Install this module alongside the Peach Payments hosted payments module by running:

```
composer require peachpayments/magento2-graphql-plugin
```

## GraphQL

To allow for a GraphQL flow, use the core Magento `setPaymentMethodOnCart` and `placeOrder` methods.

After a successful order ID has been returned, use the `getPeachHostedRedirectUrl` method to redirect the customer to the Checkout page. At this stage, the order should be in the pending state. Deconstruct the JSON object from `form_data` and submit it as `_POST` parameters to the specified `form_link` URL.

After the customer has returned to the `return_url` you specified, call the `getPeachHostedOrderStatus` method to get a success (`1`) or declined (`2`) status. If the status code is `3`, retry regularly.

### Examples

```graphql

# Set Peach Payments as payment method
mutation setPaymentMethodOnCart($cartId: String!){
  setPaymentMethodOnCart(input: {
      cart_id: $cartId
      payment_method: {
          code: "peachpayments_hosted_card"
      }
  }) {
    cart {
      selected_payment_method {
        code
      }
    }
  }
}

# Place order
mutation placeOrder($cartId: String!) {
  placeOrder(input: {cart_id: $cartId}) {
    order {
      order_number
    }
  }
}

# Get redirect URL and data
query getPeachHostedRedirectUrl($cartId: String!){
  getPeachHostedRedirectUrl(input: {
    cart_id: $cartId,
    return_url: "https://my.app.pwa/payment/welcome-back.html"
  }) {
    form_data
    form_link
  }
}

# Get status by order increment ID
query getPeachHostedOrderStatus($orderNumber: String!){
  getPeachHostedOrderStatus(input: { order_number: $orderNumber }) {
    status
  }
}

# Get order details, used for confirmation page
query getPeachHostedOrderDetailsData($cartId: String!) {
    getPeachHostedOrderDetailsData(cart_id: $cartId) {
        id
        ...OrderConfirmationPageFragment
    }
}

```
