<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\HostedGraphQl\Plugin\Resolver;

use Magento\QuoteGraphQl\Model\Resolver\PlaceOrder as GraphQlPlaceOrder;
use Magento\Sales\Api\Data\OrderInterfaceFactory;

class PlaceOrder
{
    private OrderInterfaceFactory $orderInterfaceFactory;

    /**
     * @param OrderInterfaceFactory $orderInterfaceFactory
     */
    public function __construct(
        OrderInterfaceFactory $orderInterfaceFactory
    )
    {
        $this->orderInterfaceFactory = $orderInterfaceFactory;
    }

    /**
     * @param GraphQlPlaceOrder $subject
     * @param array $result
     * @return array
     */
    public function afterResolve(
        GraphQlPlaceOrder $subject,
        array             $result
    ): array
    {
        $order = $this->orderInterfaceFactory->create()
            ->loadByIncrementId($result['order']['order_number']);
        if ($order && $order->getEntityId()) {
            $result['order']['payment_method_code'] = $order->getPayment()->getMethod();
        }

        return $result;
    }
}
