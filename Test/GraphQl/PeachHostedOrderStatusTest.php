<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\HostedGraphQl\Test\GraphQl;

use Magento\TestFramework\TestCase\GraphQlAbstract;

class PeachHostedOrderStatusTest extends GraphQlAbstract
{
    /**
     * Test getPeachHostedOrderStatus query
     */
    public function testGetPeachHostedOrderStatus()
    {
        $orderNumber = 123456;
        $query
            = <<<QUERY
        query {
            getPeachHostedOrderStatus(input: {order_number: "{$orderNumber}"}) {
                status
            }
        }
        QUERY;

        try {
            $response = $this->graphQlQuery($query);
        } catch (\Exception $e) {
            $this->fail('GraphQL query failed: ' . $e->getMessage());
        }

        $this->assertArrayHasKey('getPeachHostedOrderStatus', $response);
        $this->assertArrayHasKey('status', $response['getPeachHostedOrderStatus']);
        $this->assertContains($response['getPeachHostedOrderStatus']['status'], [1, 2, 3]);
    }
}
