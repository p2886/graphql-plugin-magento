<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\HostedGraphQl\Test\GraphQl;

use Magento\TestFramework\TestCase\GraphQlAbstract;

class PeachHostedOrderDetailsDataTest extends GraphQlAbstract
{
    /**
     * Test getPeachHostedOrderDetailsData query
     */
    public function testGetPeachHostedOrderDetailsData()
    {
        $cartId = '123456';
        $query = <<<QUERY
            query {
                getPeachHostedOrderDetailsData(cart_id: "{$cartId}") {
                    billing_address {
                        city
                        company
                        firstname
                        lastname
                        postcode
                        street
                        telephone
                        uid
                        vat_id
                    }
                    email
                    id
                    is_virtual
                    selected_payment_method {
                        code
                        purchase_order_number
                        title
                    }
                    total_quantity
                }
            }
        QUERY;

        try {
            $response = $this->graphQlQuery($query);
        } catch (\Exception $e) {
            $this->fail('GraphQL query failed: ' . $e->getMessage());
        }

        $this->assertArrayHasKey('data', $response);
        $this->assertArrayHasKey('getPeachHostedOrderDetailsData', $response['data']);
        $responseData = $response['data']['getPeachHostedOrderDetailsData'];

        if (isset($response['errors'])) {
            $this->assertArrayHasKey('errors', $response);
        } else {
            $this->assertArrayHasKey('id', $responseData);
            $this->assertArrayHasKey('billing_address', $responseData);
            $this->assertArrayHasKey('email', $responseData);
            $this->assertArrayHasKey('selected_payment_method', $responseData);
            $this->assertArrayHasKey('total_quantity', $responseData);
        }
    }
}
