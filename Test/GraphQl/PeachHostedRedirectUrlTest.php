<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\HostedGraphQl\Test\GraphQl;

use Magento\TestFramework\TestCase\GraphQlAbstract;

class PeachHostedRedirectUrlTest extends GraphQlAbstract
{
    /**
     * Test getPeachHostedRedirectUrl query
     *
     * @return void
     */
    public function testGetPeachHostedRedirectUrl()
    {
        /**
         * For testing success case is required to set existed data in variables:
         *  - $cartId
         *  - $returnUrl
         */
        $cartId = '123456';
        $returnUrl = 'https://example.com/return';
        $query = <<<QUERY
        {
            getPeachHostedRedirectUrl(input: {cart_id: "{$cartId}", return_url: "{$returnUrl}"}) {
                form_link
                form_data
            }
        }
        QUERY;

        try {
            $response = $this->graphQlQuery($query);
        } catch (\Exception $e) {
            $this->fail('GraphQL query failed: ' . $e->getMessage());
        }

        $this->assertArrayHasKey('data', $response);
        $this->assertArrayHasKey('getPeachHostedRedirectUrl', $response['data']);
        $responseData = $response['data']['getPeachHostedRedirectUrl'];

        if (isset($response['errors'])) {
            $this->assertArrayHasKey('errors', $response);
        } else {
            $this->assertArrayHasKey('form_link', $responseData);
            $this->assertArrayHasKey('form_data', $responseData);
        }
    }
}
