<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\HostedGraphQl\Test\Unit\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\GraphQl\Model\Query\Context;
use PeachPayments\Hosted\Model\Web\Hooks;
use PeachPayments\Hosted\Model\Web\HooksFactory;
use PeachPayments\HostedGraphQl\Model\Resolver\PeachHostedOrderStatus;
use PHPUnit\Framework\TestCase;

class PeachHostedOrderStatusTest extends TestCase
{
    private PeachHostedOrderStatus $object;
    private HooksFactory $webHooksFactoryMock;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->webHooksFactoryMock = $this->createMock(HooksFactory::class);
        $this->object = new PeachHostedOrderStatus(
            $this->webHooksFactoryMock
        );

        $this->fieldMock = $this->createMock(Field::class);
        $this->contextMock = $this->createMock(Context::class);
        $this->resolveInfoMock = $this->createMock(ResolveInfo::class);
        $this->webHooksMock = $this->createMock(Hooks::class);
    }

    /**
     * @return void
     */
    public function testResolve(): void
    {
        $this->webHooksFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->webHooksMock);
        $this->webHooksMock->expects($this->once())
            ->method('loadByIncrementId')
            ->willReturnSelf();
        $this->webHooksMock->expects($this->atLeast(1))
            ->method('getData')
            ->with('result_code')
            ->willReturn('000.000.000');
        $this->webHooksMock->expects($this->once())
            ->method('getId')
            ->willReturn(123456);

        $result = $this->object->resolve(
            $this->fieldMock,
            $this->contextMock,
            $this->resolveInfoMock,
            [],
            ['input' => ['order_number' => '123456']]
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('status', $result);
    }

    /**
     * @return void
     */
    public function testResolveEmptyResultCode(): void
    {
        $this->webHooksFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->webHooksMock);
        $this->webHooksMock->expects($this->once())
            ->method('loadByIncrementId')
            ->willReturnSelf();
        $this->webHooksMock->expects($this->once())
            ->method('getData')
            ->with('result_code')
            ->willReturn(null);

        $result = $this->object->resolve(
            $this->fieldMock,
            $this->contextMock,
            $this->resolveInfoMock,
            [],
            ['input' => ['order_number' => '123456']]
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('status', $result);
        $this->assertEquals(['status' => 3], $result);
    }
}
