<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\HostedGraphQl\Test\Unit\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\GraphQl\Model\Query\Context;
use Magento\GraphQl\Model\Query\ContextExtensionInterface;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\CartInterface;
use Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface;
use Magento\Store\Api\Data\StoreInterface;
use PeachPayments\HostedGraphQl\Model\Resolver\PeachHostedOrderDetailsData;
use PHPUnit\Framework\TestCase;

class PeachHostedOrderDetailsDataTest extends TestCase
{
    private PeachHostedOrderDetailsData $object;
    private MaskedQuoteIdToQuoteIdInterface $maskedQuoteIdToQuoteIdMock;
    private CartRepositoryInterface $cartRepositoryMock;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->maskedQuoteIdToQuoteIdMock = $this->createMock(MaskedQuoteIdToQuoteIdInterface::class);
        $this->cartRepositoryMock = $this->createMock(CartRepositoryInterface::class);
        $this->object = new PeachHostedOrderDetailsData(
            $this->maskedQuoteIdToQuoteIdMock,
            $this->cartRepositoryMock
        );

        $this->fieldMock = $this->createMock(Field::class);
        $this->contextMock = $this->createMock(Context::class);
        $this->resolveInfoMock = $this->createMock(ResolveInfo::class);
        $this->contextExtensionMock = $this->createMock(ContextExtensionInterface::class);
        $this->storeMock = $this->createMock(StoreInterface::class);
        $this->cartMock = $this->getMockBuilder(CartInterface::class)
            ->disableOriginalConstructor()
            ->addMethods(['getCustomerId'])
            ->getMockForAbstractClass();
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testResolve()
    {
        $this->contextMock->expects($this->once())
            ->method('getUserId')
            ->willReturn(123);
        $this->contextMock->expects($this->once())
            ->method('getExtensionAttributes')
            ->willReturn($this->contextExtensionMock);
        $this->contextExtensionMock->expects($this->once())
            ->method('getStore')
            ->willReturn($this->storeMock);
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->willReturn(1);
        $this->maskedQuoteIdToQuoteIdMock->expects($this->once())
            ->method('execute')
            ->willReturn(123456);
        $this->cartRepositoryMock->expects($this->once())
            ->method('get')
            ->willReturn($this->cartMock);
        $this->cartMock->expects($this->once())
            ->method('getStoreId')
            ->willReturn(1);
        $this->cartMock->expects($this->once())
            ->method('getCustomerId')
            ->willReturn(123);

        $result = $this->object->resolve(
            $this->fieldMock,
            $this->contextMock,
            $this->resolveInfoMock,
            [],
            ['cart_id' => "dwai129102miDAWPODm"]
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('model', $result);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testResolveCartIdMissing()
    {
        $this->expectException('\Magento\Framework\GraphQl\Exception\GraphQlInputException');
        $this->expectExceptionMessage('Required parameter "cart_id" is missing');
        $this->object->resolve(
            $this->fieldMock,
            $this->contextMock,
            $this->resolveInfoMock,
            [],
            []
        );
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testResolveCannotFindCartByMaskId()
    {
        $this->expectException('\Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException');

        $this->contextMock->expects($this->once())
            ->method('getUserId')
            ->willReturn(123);
        $this->contextMock->expects($this->once())
            ->method('getExtensionAttributes')
            ->willReturn($this->contextExtensionMock);
        $this->contextExtensionMock->expects($this->once())
            ->method('getStore')
            ->willReturn($this->storeMock);
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->willReturn(1);
        $this->maskedQuoteIdToQuoteIdMock->expects($this->once())
            ->method('execute')
            ->willThrowException(new GraphQlNoSuchEntityException(__("Test Error")));

        $this->object->resolve(
            $this->fieldMock,
            $this->contextMock,
            $this->resolveInfoMock,
            [],
            ['cart_id' => "dwai129102miDAWPODm"]
        );
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testResolveCannotFindCartByCartId()
    {
        $this->expectException('\Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException');

        $this->contextMock->expects($this->once())
            ->method('getUserId')
            ->willReturn(123);
        $this->contextMock->expects($this->once())
            ->method('getExtensionAttributes')
            ->willReturn($this->contextExtensionMock);
        $this->contextExtensionMock->expects($this->once())
            ->method('getStore')
            ->willReturn($this->storeMock);
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->willReturn(1);
        $this->maskedQuoteIdToQuoteIdMock->expects($this->once())
            ->method('execute')
            ->willReturn(123456);
        $this->cartRepositoryMock->expects($this->once())
            ->method('get')
            ->willThrowException(new GraphQlNoSuchEntityException(__("Test Error")));

        $this->object->resolve(
            $this->fieldMock,
            $this->contextMock,
            $this->resolveInfoMock,
            [],
            ['cart_id' => "dwai129102miDAWPODm"]
        );
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testResolveAuthorizationError()
    {
        $this->expectException('\Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException');

        $this->contextMock->expects($this->once())
            ->method('getUserId')
            ->willReturn(123);
        $this->contextMock->expects($this->once())
            ->method('getExtensionAttributes')
            ->willReturn($this->contextExtensionMock);
        $this->contextExtensionMock->expects($this->once())
            ->method('getStore')
            ->willReturn($this->storeMock);
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->willReturn(1);
        $this->maskedQuoteIdToQuoteIdMock->expects($this->once())
            ->method('execute')
            ->willReturn(123456);
        $this->cartRepositoryMock->expects($this->once())
            ->method('get')
            ->willReturn($this->cartMock);
        $this->cartMock->expects($this->once())
            ->method('getStoreId')
            ->willReturn(1);
        $this->cartMock->expects($this->once())
            ->method('getCustomerId')
            ->willReturn(321);

        $this->object->resolve(
            $this->fieldMock,
            $this->contextMock,
            $this->resolveInfoMock,
            [],
            ['cart_id' => "dwai129102miDAWPODm"]
        );
    }
}
