<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\HostedGraphQl\Test\Unit\Model\Resolver;

use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\GraphQl\Model\Query\Context;
use Magento\GraphQl\Model\Query\ContextExtensionInterface;
use Magento\Quote\Model\MaskedQuoteIdToQuoteIdInterface;
use Magento\Sales\Api\Data\OrderAddressInterface;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order;
use Magento\Sales\Model\ResourceModel\Order\Collection;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactoryInterface;
use Magento\Store\Api\Data\StoreInterface;
use PeachPayments\Hosted\Helper\Config as ConfigHelper;
use PeachPayments\Hosted\Helper\Data as Helper;
use PeachPayments\Hosted\Model\Web\Hooks;
use PeachPayments\Hosted\Model\Web\HooksFactory;
use PeachPayments\HostedGraphQl\Model\Resolver\PeachHostedRedirectUrl;
use PHPUnit\Framework\TestCase;

class PeachHostedRedirectUrlTest extends TestCase
{
    private PeachHostedRedirectUrl $object;
    private CollectionFactoryInterface $orderCollectionFactoryMock;
    private MaskedQuoteIdToQuoteIdInterface $maskedQuoteIdToQuoteIdMock;
    private Helper $helperMock;
    private HooksFactory $webHooksFactoryMock;
    private ConfigHelper $configHelperMock;

    /**
     * @return void
     * @throws \PHPUnit\Framework\MockObject\Exception
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->orderCollectionFactoryMock = $this->createMock(CollectionFactoryInterface::class);
        $this->maskedQuoteIdToQuoteIdMock = $this->createMock(MaskedQuoteIdToQuoteIdInterface::class);
        $this->helperMock = $this->createMock(Helper::class);
        $this->webHooksFactoryMock = $this->createMock(HooksFactory::class);
        $this->configHelperMock = $this->createMock(ConfigHelper::class);

        $this->object = new PeachHostedRedirectUrl(
            $this->maskedQuoteIdToQuoteIdMock,
            $this->orderCollectionFactoryMock,
            $this->helperMock,
            $this->configHelperMock,
            $this->webHooksFactoryMock
        );

        $this->fieldMock = $this->createMock(Field::class);
        $this->contextMock = $this->createMock(Context::class);
        $this->resolveInfoMock = $this->createMock(ResolveInfo::class);
        $this->webHooksMock = $this->createMock(Hooks::class);
        $this->contextExtensionMock = $this->createMock(ContextExtensionInterface::class);
        $this->storeMock = $this->createMock(StoreInterface::class);
        $this->orderCollectionMock = $this->createMock(Collection::class);
        $this->orderMock = $this->createMock(Order::class);
        $this->paymentMock = $this->createMock(OrderPaymentInterface::class);
        $this->orderAddressMock = $this->createMock(OrderAddressInterface::class);
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function testResolve(): void
    {
        $this->contextMock->expects($this->once())
            ->method('getUserId')
            ->willReturn(123);
        $this->contextMock->expects($this->once())
            ->method('getExtensionAttributes')
            ->willReturn($this->contextExtensionMock);
        $this->contextExtensionMock->expects($this->once())
            ->method('getStore')
            ->willReturn($this->storeMock);
        $this->storeMock->expects($this->once())
            ->method('getId')
            ->willReturn(1);
        $this->maskedQuoteIdToQuoteIdMock->expects($this->once())
            ->method('execute')
            ->willReturn(123456);
        $this->helperMock->expects($this->once())
            ->method('getCheckoutUrl')
            ->willReturn('example.mode.com');
        $this->helperMock->expects($this->once())
            ->method('signData')
            ->willReturn('test-code-for-form-data');
        $this->orderCollectionFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->orderCollectionMock);
        $this->orderCollectionMock->expects($this->any())
            ->method('addFilter')
            ->willReturnSelf();
        $this->orderCollectionMock->expects($this->once())
            ->method('getTotalCount')
            ->willReturn(1);
        $this->orderCollectionMock->expects($this->once())
            ->method('getFirstItem')
            ->willReturn($this->orderMock);
        $this->orderMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->once())
            ->method('getAmountOrdered')
            ->willReturn(20.0000);
        $this->orderMock->expects($this->any())
            ->method('getBillingAddress')
            ->willReturn($this->orderAddressMock);
        $this->orderAddressMock->expects($this->any())
            ->method('getStreet')
            ->willReturn(['Test Street']);
        $this->orderAddressMock->expects($this->any())
            ->method('getCountryId')
            ->willReturn('US');
        $this->orderMock->expects($this->atLeast(2))
            ->method('getId')
            ->willReturn('123456');
        $this->orderMock->expects($this->once())
            ->method('getRealOrderId')
            ->willReturn('real-123456');
        $this->webHooksFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->webHooksMock);
        $this->webHooksMock->expects($this->once())
            ->method('loadByOrderId')
            ->willReturnSelf();
        $this->webHooksMock->expects($this->any())
            ->method('getId')
            ->willReturn(123);
        $this->webHooksMock->expects($this->any())
            ->method('addData')
            ->willReturnSelf();
        $this->webHooksMock->expects($this->any())
            ->method('setData')
            ->willReturnSelf();
        $this->webHooksMock->expects($this->once())
            ->method('save')
            ->willReturnSelf();
        $this->configHelperMock->expects($this->once())
            ->method('getEntityId3DSecure')
            ->willReturn('3d-secure-code');
        $this->orderMock->expects($this->once())
            ->method('getOrderCurrencyCode')
            ->willReturn('USD');
        $this->orderMock->expects($this->once())
            ->method('getIncrementId')
            ->willReturn('real-123456');
        $this->helperMock->expects($this->once())
            ->method('getPlatformName')
            ->willReturn('MAGENTO');
        $this->orderAddressMock->expects($this->once())
            ->method('getFirstname')
            ->willReturn('Firstname');
        $this->orderAddressMock->expects($this->once())
            ->method('getLastname')
            ->willReturn('Lastname');
        $this->orderAddressMock->expects($this->once())
            ->method('getTelephone')
            ->willReturn('0852146379');
        $this->orderAddressMock->expects($this->once())
            ->method('getEmail')
            ->willReturn('example@test.com');
        $this->orderMock->expects($this->once())
            ->method('getCustomerIsGuest')
            ->willReturn(false);
        $this->orderAddressMock->expects($this->any())
            ->method('getCity')
            ->willReturn('Test');
        $this->orderAddressMock->expects($this->once())
            ->method('getPostcode')
            ->willReturn('12345');
        $this->orderMock->expects($this->any())
            ->method('getShippingAddress')
            ->willReturn($this->orderAddressMock);


        $result = $this->object->resolve(
            $this->fieldMock,
            $this->contextMock,
            $this->resolveInfoMock,
            [],
            ['input' => ['cart_id' => '123456', 'return_url' => 'example.com']]
        );

        $this->assertIsArray($result);
        $this->assertArrayHasKey('form_link', $result);
        $this->assertArrayHasKey('form_data', $result);
    }
}
