<?php
/**
 * @author    X2Y.io Dev Team
 * @copyright Copyright (c) X2Y.io, Inc. (https://x2y.io/)
 */

declare(strict_types=1);

namespace PeachPayments\HostedGraphQl\Test\Unit\Plugin\Resolver;

use Magento\Sales\Api\Data\OrderInterfaceFactory;
use Magento\Sales\Api\Data\OrderPaymentInterface;
use Magento\Sales\Model\Order;
use PeachPayments\HostedGraphQl\Plugin\Resolver\PlaceOrder;
use PHPUnit\Framework\MockObject\Exception;
use PHPUnit\Framework\TestCase;

class PlaceOrderTest extends TestCase
{
    private PlaceOrder $object;
    private OrderInterfaceFactory $orderInterfaceFactoryMock;

    /**
     * @return void
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->orderInterfaceFactoryMock = $this->createMock(OrderInterfaceFactory::class);
        $this->object = new PlaceOrder($this->orderInterfaceFactoryMock);

        $this->orderMock = $this->createMock(Order::class);
        $this->paymentMock = $this->createMock(OrderPaymentInterface::class);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testAfterResolve()
    {
        $this->orderInterfaceFactoryMock->expects($this->once())
            ->method('create')
            ->willReturn($this->orderMock);
        $this->orderMock->expects($this->once())
            ->method('loadByIncrementId')
            ->with('100001')
            ->willReturnSelf();
        $this->orderMock->expects($this->once())
            ->method('getEntityId')
            ->willReturn('100001');
        $this->orderMock->expects($this->once())
            ->method('getPayment')
            ->willReturn($this->paymentMock);
        $this->paymentMock->expects($this->once())
            ->method('getMethod')
            ->willReturn("test_payment_method");

        $subject = $this->createMock(\Magento\QuoteGraphQl\Model\Resolver\PlaceOrder::class);
        $result = $this->object->afterResolve(
            $subject,
            ['order' => ['order_number' => '100001']]
        );

        $this->assertIsArray($result);
    }
}
